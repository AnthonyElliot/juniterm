﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

namespace JUniterm.Consts
{
    public static class FontConsts
    {
        public static List<string> GetFontFamilies()
        {
            List<string> fontFamilies = new List<string>();
            foreach (var font in Fonts.SystemFontFamilies)
            {
                var names = font.FamilyNames.Values;
                foreach (var name in names)
                {
                    fontFamilies.Add(name);
                }
            }
            return fontFamilies;
        }

        public static List<short> GetFontSizes()
            => Enumerable.Range(8, 65).Select(x => (short) x).ToList();
    }
}
