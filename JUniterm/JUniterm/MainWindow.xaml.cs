﻿using JUniterm.Pages;
using JUniterm.Pages.Canvas;
using System.Windows;

namespace JUniterm
{
    public partial class MainWindow : Window
    {
        private MainDataContext context;
        public MainWindow()
        {
            InitializeComponent();

            context = new MainDataContext
            {
                MainFrame = MenuFrame,
                Selected = null,
                CanvasPage = new CanvasPage()
            };

            MenuFrame.Content = new MainMenuPage(context);
            CanvasFrame.Content = context.CanvasPage;
        }
    }
}
