﻿using JUniterm.Database.Models;
using JUniterm.Models;
using System;
using System.Diagnostics;
using System.Windows.Media;

namespace JUniterm.Converters
{
    public static class DrawingModelConverter
    {
        public static DrawingModel GetDrawingModel(Uniterm uniterm)
        {
            return new DrawingModel
               {
                   SA = uniterm.FirstOperatorA,
                   SB = uniterm.FirstOperatorB,
                   SC = uniterm.FirstOperatorC,
                   SOp = uniterm.FirstOperator,
                   EA = uniterm.SecondOperatorA,
                   EB = uniterm.SecondOperatorB,
                   EOp = uniterm.SecondOperator,
                   Swap = uniterm.Switched,
                   FontFamily = new FontFamily(uniterm.FontFamily),
                   FontSize = uniterm.FontSize,
                   Pen = new Pen(Brushes.SteelBlue, Math.Log(uniterm.FontSize, 5))
               };
        }
            
    }
}
