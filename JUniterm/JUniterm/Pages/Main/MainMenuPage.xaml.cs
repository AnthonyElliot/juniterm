﻿using JUniterm.Controls;
using JUniterm.Converters;
using JUniterm.Pages.Fonts;
using JUniterm.Pages.Operators;
using JUniterm.Pages.Swap;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace JUniterm.Pages
{
    public partial class MainMenuPage : Page
    {
        private MainDataContext context;
        public MainMenuPage(MainDataContext context)
        {
            this.context = context;
            InitializeComponent();
            SetEnabled();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private void DatabaseTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new DatabasePage(context);

        private void FirstOperatorsTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new FirstOperatorsPage(context);

        private void SecondOperatorsTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new SecondOperatorsPage(context);

        private void TextTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new FontPage(context);

        private void SwapTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new SwapPage(context);

        private void SetEnabled()
        {
            if (context.Selected is null)
            {
                for (int i = 1; i < TilePanel.Children.Count; ++i)
                {
                    TilePanel.Children[i].IsEnabled = false;
                    (TilePanel.Children[i] as ImageTile).Color = new SolidColorBrush(Colors.LightGray);
                }
            }
        }

        private void RedrawTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.CanvasPage.Draw(DrawingModelConverter.GetDrawingModel(context.Selected));

        private void ClearTile_MouseUp(object sender, MouseButtonEventArgs e)
            => context.CanvasPage.MyCanvas.ClearAll();
    }
}
