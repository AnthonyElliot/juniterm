﻿using JUniterm.Converters;
using JUniterm.Database.Models;
using System.Windows.Controls;
using System.Windows.Input;

namespace JUniterm.Pages.Fonts
{
    public partial class FontPage : Page
    {
        private MainDataContext context;
        private UnitermEditVM unitermEditVM;

        public FontPage(MainDataContext context)
        {
            this.context = context;
            unitermEditVM = context.UnitermService.GetUnitermEditVM(context.Selected.Id);
            InitializeComponent();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private void BackTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new MainMenuPage(context);

        private void CleanTile_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FontFamilyTile.ClearSelectedItem();
            FontSizeTile.ClearSelectedItem();
        }

        private void SaveTile_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(context.Selected != null)
            {
                unitermEditVM.FontFamily = (string)FontFamilyTile.GetSelectedValue();
                unitermEditVM.FontSize = (short)FontSizeTile.GetSelectedValue();
                context.UnitermService.Update(unitermEditVM);
                context.Selected = context.UnitermService.GetById(unitermEditVM.Id);
                context.CanvasPage.Draw(DrawingModelConverter.GetDrawingModel(context.Selected));
                context.MainFrame.Content = new MainMenuPage(context);
            }
        }

        private void FontFamilyTile_Loaded(object sender, System.Windows.RoutedEventArgs e) 
            => FontFamilyTile.SetDefaultFontFamily(unitermEditVM.FontFamily);

        private void FontSizeTile_Loaded(object sender, System.Windows.RoutedEventArgs e) 
            => FontSizeTile.SetDefaultFontSize(unitermEditVM.FontSize);
    }
}
