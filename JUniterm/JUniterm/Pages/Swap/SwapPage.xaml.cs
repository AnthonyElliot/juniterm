﻿using JUniterm.Converters;
using JUniterm.Database.Models;
using JUniterm.Enums;
using System.Windows.Controls;
using System.Windows.Input;

namespace JUniterm.Pages.Swap
{
    public partial class SwapPage : Page
    {
        private MainDataContext context;
        private UnitermEditVM unitermEditVM;

        public SwapPage(MainDataContext context)
        {
            this.context = context;
            unitermEditVM = context.UnitermService.GetUnitermEditVM(context.Selected.Id);
            InitializeComponent();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private void BackTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new MainMenuPage(context);

        private void FirstOperatorTile_MouseUp(object sender, MouseButtonEventArgs e)
            => Save(OperatorEnum.A);

        private void SecondOperatorTile_MouseUp(object sender, MouseButtonEventArgs e)
            => Save(OperatorEnum.B);

        private void TextTile_MouseUp(object sender, MouseButtonEventArgs e)
            => Save(OperatorEnum.C);

        private void Save(OperatorEnum operatorEnum)
        {
            switch(operatorEnum)
            {
                case OperatorEnum.A:
                    unitermEditVM.Switched = "A";
                    break;
                case OperatorEnum.B:
                    unitermEditVM.Switched = "B";
                    break;
                case OperatorEnum.C:
                    unitermEditVM.Switched = "C";
                    break;
                case OperatorEnum.None:
                    unitermEditVM.Switched = null;
                    break;
            }

            context.UnitermService.Update(unitermEditVM);
            context.Selected = context.UnitermService.GetById(unitermEditVM.Id);
            context.CanvasPage.Draw(DrawingModelConverter.GetDrawingModel(context.Selected));
            context.MainFrame.Content = new MainMenuPage(context);
        }

        private void NoneTile_MouseUp(object sender, MouseButtonEventArgs e)
            => Save(OperatorEnum.None);
    }
}
