﻿using JUniterm.Models;
using System.Windows.Controls;
using System.Windows.Media;

namespace JUniterm.Pages.Canvas
{
    public partial class CanvasPage : Page
    {
        public CanvasPage() => InitializeComponent();

        public void Draw(DrawingModel model)
        {
            MyCanvas.ClearAll();
            DrawingVisual drawingVisual = new DrawingVisual();
            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                MyDrawing myDrawing = new MyDrawing(drawingContext, model, MyCanvas);
                myDrawing.Redraw();
                drawingContext.Close();
            }
            MyCanvas.AddItem(drawingVisual);
        }
    }
}
