﻿using JUniterm.Database.Models;
using System.Windows.Controls;
using System.Windows.Input;
using JUniterm.Converters;

namespace JUniterm.Pages.Operators
{
    public partial class SecondOperatorsPage : Page
    {
        private MainDataContext context;
        private UnitermEditVM unitermEditVM;

        public SecondOperatorsPage(MainDataContext context)
        {
            this.context = context;
            unitermEditVM = context.UnitermService.GetUnitermEditVM(context.Selected.Id);
            InitializeComponent();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private void BackTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new MainMenuPage(context);

        private void CleanTile_MouseUp(object sender, MouseButtonEventArgs e)
        {
            OperatorATile.InputValue = OperatorBTile.InputValue = null;
            OperationTile.ResetOperation();
        }

        private void OperatorATile_Loaded(object sender, System.Windows.RoutedEventArgs e)
            => OperatorATile.InputValue = unitermEditVM.SecondOperatorA;

        private void OperatorBTile_Loaded(object sender, System.Windows.RoutedEventArgs e)
            => OperatorBTile.InputValue = unitermEditVM.SecondOperatorB;

        private void OperationTile_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (unitermEditVM.SecondOperator != null)
            {
                var isSemicolon = (unitermEditVM.SecondOperator == ";");
                OperationTile.SetOperation(isSemicolon);
            }
        }

        private void SaveTile_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (context.Selected != null && (IsEmpty() || IsValid()))
            {
                unitermEditVM.SecondOperatorA = OperatorATile.InputValue;
                unitermEditVM.SecondOperatorB = OperatorBTile.InputValue;
                unitermEditVM.SecondOperator = (IsEmpty()) ? null : (OperationTile.IsCommaSelected()) ? "," : ";";
                context.UnitermService.Update(unitermEditVM);
                context.Selected = context.UnitermService.GetById(unitermEditVM.Id);
                context.CanvasPage.Draw(DrawingModelConverter.GetDrawingModel(context.Selected));
                context.MainFrame.Content = new MainMenuPage(context);
            }
        }

        private bool IsValid()
            => (OperatorATile.IsValid() && OperatorBTile.IsValid());

        private bool IsEmpty()
            => (OperatorATile.IsEmpty() && OperatorBTile.IsEmpty());
    }
}
