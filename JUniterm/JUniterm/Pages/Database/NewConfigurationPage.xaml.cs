﻿using JUniterm.Consts;
using JUniterm.Database.Models;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace JUniterm.Pages.Database
{
    public partial class NewConfigurationPage : Page
    {
        private MainDataContext context;
        private object model;

        public NewConfigurationPage(MainDataContext context, object model)
        {
            InitializeComponent();
            this.context = context;
            this.model = model;
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private void BackTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new DatabasePage(context);

        private void CleanTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => NameTile.InputValue = DescriptionTile.InputValue = string.Empty;

        private void SaveTile_MouseUp(object sender, MouseButtonEventArgs e)
        {
            switch (model)
            {
                case UnitermAddVM addModel:
                    addModel.Name = NameTile.InputValue;
                    addModel.Description = DescriptionTile.InputValue;
                    addModel.FontFamily = FontConsts.GetFontFamilies().First();
                    addModel.FontSize = FontConsts.GetFontSizes().First();
                    addModel.FirstOperator = addModel.SecondOperator = ";";
                    context.UnitermService.Add(addModel);
                    break;
                case UnitermEditVM editVM:
                    editVM.Name = NameTile.InputValue;
                    editVM.Description = DescriptionTile.InputValue;
                    context.UnitermService.Update(editVM);
                    break;
            }
            context.MainFrame.Content = new DatabasePage(context);
        }

        private void NameTile_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            switch(model)
            {
                case UnitermEditVM x:
                    NameTile.InputValue = x.Name;
                    break;
            }
        }

        private void DescriptionTile_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            switch (model)
            {
                case UnitermEditVM x:
                    DescriptionTile.InputValue = x.Description;
                    break;
            }
        }
    }
}
