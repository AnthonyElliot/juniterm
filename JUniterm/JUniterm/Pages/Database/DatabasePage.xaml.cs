﻿using JUniterm.Controls;
using JUniterm.Controls.Tile;
using JUniterm.Converters;
using JUniterm.Database.Models;
using JUniterm.Pages.Database;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace JUniterm.Pages
{
    public partial class DatabasePage : Page
    {
        private MainDataContext context;

        public DatabasePage(MainDataContext context)
        {
            InitializeComponent();
            this.context = context;
            TilePanel.Children.Add(CreateAddTile());
            BackTile.IsEnabled = (LoadAllUnitermTiles() > 0);
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private ImageTile CreateAddTile()
        {
            ImageTile tile = new ImageTile
            {
                ImageSource = "/Assets/plus.png",
                Color = new SolidColorBrush(Colors.White),
                SubHeader = "Nowy",
                Descritpion = "Dodaj nową konfigurację",
            };
            tile.MouseUp += AddTile_MouseUp;
            return tile;
        }

        private void BackTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new MainMenuPage(context);

        private void AddTile_MouseUp(object sender, MouseButtonEventArgs e) 
            => context.MainFrame.Content = new NewConfigurationPage(context, new UnitermAddVM());

        private int LoadAllUnitermTiles()
        {
            var uniterms = context.UnitermService.GetAllUnitermDTOs();
            foreach(var item in uniterms)
            {
                var tile = TextActionTile.GetTextActionTile(item.Id.ToString(), item.Name, item.Description);
                tile.SelectAction += UnitermTile_Selected;
                tile.EditAction += UnitermTile_Edit;
                tile.RemoveAction += UnitermTile_Remove;
                if(context.Selected != null && context.Selected.Id == item.Id)
                {
                    tile.Color = new SolidColorBrush(Colors.Azure);
                }
                TilePanel.Children.Add(tile);
            }
            return uniterms.Count;
        }

        private void UnitermTile_Selected(object sender)
        {
            var item = sender as TextActionTile;
            if(int.TryParse(item.Header, out int id))
            {
                context.Selected = context.UnitermService.GetById(id);
                context.MainFrame.Content = new MainMenuPage(context);
                context.CanvasPage.Draw(DrawingModelConverter.GetDrawingModel(context.Selected));
            }
        }

        private void UnitermTile_Edit(object sender)
        {
            var item = sender as TextActionTile;
            if (int.TryParse(item.Header, out int id))
            {
                var toEdit = context.UnitermService.GetUnitermEditVM(id);
                context.MainFrame.Content = new NewConfigurationPage(context, toEdit);
            }
        }

        private void UnitermTile_Remove(object sender)
        {
            var item = sender as TextActionTile;
            if(int.TryParse(item.Header, out int id))
            {
                if(context.Selected != null && context.Selected.Id == id)
                {
                    context.Selected = null;
                    context.CanvasPage.MyCanvas.ClearAll();
                }
                context.UnitermService.DeleteById(id);
                TilePanel.Children.Remove(sender as UIElement);
            }
        }
    }
}
