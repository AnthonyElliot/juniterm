﻿using JUniterm.Database;
using JUniterm.Database.Models;
using JUniterm.Database.Repositories;
using JUniterm.Database.Services;
using JUniterm.Pages.Canvas;
using System.Windows.Controls;

namespace JUniterm
{
    public class MainDataContext
    {
        public Frame MainFrame { get; set; }
        public Uniterm Selected { get; set; }
        public CanvasPage CanvasPage { get; set; } 
        public IUnitermService UnitermService { get; } = new UnitermService(new UnitermRepository(new UnitermContext()));
    }
}
