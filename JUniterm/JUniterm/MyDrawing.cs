﻿using JUniterm.Controls.Canvas;
using JUniterm.Models;
using System;
using System.Windows;
using System.Windows.Media;

namespace JUniterm
{
    public class MyDrawing
    {
        public DrawingContext DrawingContext { get; set; }
        private DrawingModel dm;
        private MathCanvas canvas;

        public MyDrawing(DrawingContext drawingContext, DrawingModel drawingModel, MathCanvas canvas)
        {
            DrawingContext = drawingContext;
            this.dm = drawingModel;
            this.canvas = canvas;
        }

        public void Redraw()
        {
            if (dm.Swap != null)
            {
                DrawSwitched(new Point(20, dm.FontSize + 30));
            }
            else
            {
                if (!dm.IsSOperatorsEmpty())
                {
                    DrawElimWithText(new Point(30, dm.FontSize + 30));
                }
                if (!dm.IsEOperatorsEmpty())
                {
                    DrawParallel(new Point(30, dm.FontSize * 3 + 30));
                }
            }
        }

        public void DrawSwitched(Point pt)
        {
            if (!dm.IsOperatorsEmpty())
            {
                string textParallel = dm.GetTextForParallel();
                int length = GetTextLength(textParallel) + 2;

                dm.SOp = $"{dm.SOp} ";

                if (dm.Swap == "A")
                {
                    DrawText(new Point(pt.X + length + (dm.FontSize / 3), pt.Y + 3), dm.SOp + dm.SB + dm.SOp + dm.SC);
                    DrawParallel(new Point(pt.X + (dm.FontSize / 3), pt.Y + 3));
                    length += GetTextLength(dm.SOp + dm.SB + dm.SOp + dm.SC) + (int)(dm.FontSize / 3);
                }
                else if (dm.Swap == "B")
                {
                    DrawText(pt, dm.SA + dm.SOp);
                    Point point = new Point(pt.X + GetTextLength(dm.SA + dm.SOp) + (dm.FontSize / 3), pt.Y + 3);
                    DrawParallel(point);
                    point.X += length;
                    point.Y -= 3;
                    DrawText(point, dm.SOp + dm.SC);
                    length += GetTextLength(dm.SA + dm.SOp) + (int)(dm.FontSize / 3) + GetTextLength(dm.SOp + dm.SC);
                }
                else if (dm.Swap == "C")
                {
                    DrawText(pt, dm.SA + dm.SOp + dm.SB + dm.SOp);
                    Point point = new Point(pt.X + GetTextLength(dm.SA + dm.SOp + dm.SB + dm.SOp) + (dm.FontSize / 3), pt.Y + 3);
                    DrawParallel(point);
                    length += GetTextLength(dm.SA + dm.SOp + dm.SB + dm.SOp) + (int)(dm.FontSize / 3);
                }
                dm.SOp = Convert.ToString(dm.SOp[0]);
                DrawElim(pt, length);
            }
        }

        public void DrawElim(Point pt, double lenght = 0)
        {
            if (!dm.IsSOperatorsEmpty())
            {
                double l = (lenght == 0) ? GetTextLength(dm.SA + dm.SOp + " " + dm.SB + dm.SOp + " " + dm.SC) + 2 : lenght + 2;
                DrawHorizontally(pt, (int) l);
            }
        }

        public void DrawElimWithText(Point pt)
        {
            DrawElim(pt);
            DrawText(new Point(pt.X, pt.Y + 2), dm.SA + dm.SOp + " " + dm.SB + dm.SOp + " " + dm.SC);
        }

        public void DrawParallel(Point pt)
        {
            if (!dm.IsEOperatorsEmpty())
            {
                Point p2 = new Point(pt.X + 2, pt.Y);
                string text = dm.GetTextForParallel();
                double l = GetTextHeight(text) + 2;
                DrawText(p2, text);
                DrawVertically(pt, (int)l);
            }   
        }

        private int GetTextLength(string text) => (int)GetFormattedText(text).Width;

        private FormattedText GetFormattedText(string text)
        {
            FontStyle style = FontStyles.Normal;
            style = FontStyles.Normal;
            Typeface typeface = new Typeface(dm.FontFamily, style, FontWeights.Light, FontStretches.Medium);

            FormattedText formattedText = new FormattedText(text,
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                typeface, dm.FontSize, Brushes.Black, VisualTreeHelper.GetDpi(canvas).PixelsPerDip);

            formattedText.TextAlignment = TextAlignment.Left;
            return formattedText;
        }

        private void DrawText(Point point, string text) => DrawingContext.DrawText(GetFormattedText(text), point);

        private int GetTextHeight(string text) => (int)GetFormattedText(text).Height;

        private void DrawHorizontally(Point pt, int length)
        {
            DrawingContext.DrawLine(dm.Pen, pt, new Point { X = pt.X + length, Y = pt.Y});
            double b = (Math.Sqrt(length) / 2) + 2;
            DrawingContext.DrawLine(dm.Pen, new Point(pt.X, pt.Y - (b / 2)), new Point(pt.X, pt.Y + (b / 2)));
            DrawingContext.DrawLine(dm.Pen, new Point(pt.X + length, pt.Y - (b / 2)), new Point(pt.X + length, pt.Y + (b / 2)));
        }

        private void DrawVertically(Point pt, int length)
        {
            DrawingContext.DrawLine(dm.Pen, pt, new Point { X = pt.X, Y = pt.Y + length });
            double b = (Math.Sqrt(length) / 2) + 2;
            DrawingContext.DrawLine(dm.Pen, new Point(pt.X, pt.Y), new Point(pt.X + b, pt.Y));
            DrawingContext.DrawLine(dm.Pen, new Point(pt.X, pt.Y + length), new Point(pt.X + b, pt.Y + length));
        }
    }
}
