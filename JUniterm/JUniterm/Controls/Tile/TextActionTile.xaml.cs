﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace JUniterm.Controls.Tile
{
    public partial class TextActionTile : UserControl
    {
        public static readonly DependencyProperty SubHeaderProperty =
            DependencyProperty.Register("SubHeader", typeof(string), typeof(TextActionTile));

        public string SubHeader
        {
            get { return (string)GetValue(SubHeaderProperty); }
            set { SetValue(SubHeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(TextActionTile), null);

        public string Header
        {
            get { return GetValue(HeaderProperty) as string; }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(TextActionTile), null);

        public SolidColorBrush Color
        {
            get { return GetValue(ColorProperty) as SolidColorBrush; }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(TextActionTile), null);

        public string Descritpion
        {
            get { return GetValue(DescriptionProperty) as string; }
            set { SetValue(DescriptionProperty, value); }
        }

        private Action<object> selectAction, editAction, removeAction;

        public Action<object> SelectAction
        {
            get { return selectAction; }
            set
            {
                selectAction = value;
                OnPropertyChanged("SelectAction");
            }
        }

        public Action<object> EditAction
        {
            get { return editAction; }
            set
            {
                editAction = value;
                OnPropertyChanged("EditAction");
            }
        }

        public Action<object> RemoveAction
        {
            get { return removeAction; }
            set
            {
                removeAction = value;
                OnPropertyChanged("RemoveAction");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public TextActionTile()
        {
            InitializeComponent();
            DataContext = this;
        }

        public static TextActionTile GetTextActionTile(string header, string subHeader, string description)
            => new TextActionTile
            {
                Header = header,
                SubHeader = subHeader,
                Descritpion = description,
                Color = new SolidColorBrush(Colors.White)
            };

        protected void OnPropertyChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        private void SelectButton_OnClick() => selectAction(this);

        private void EditButton_OnClick() => editAction(this);

        private void RemoveButton_OnClick() => removeAction(this);
    }
}
