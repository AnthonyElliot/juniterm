﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace JUniterm.Controls.Tile
{
    public partial class InputImageTile : UserControl
    {
        public static readonly DependencyProperty SubHeaderProperty =
            DependencyProperty.Register("SubHeader", typeof(string), typeof(InputImageTile), null);

        public string SubHeader
        {
            get { return (string)GetValue(SubHeaderProperty); }
            set { SetValue(SubHeaderProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(string), typeof(InputImageTile), null);

        public string ImageSource
        {
            get { return GetValue(ImageSourceProperty) as string; }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(InputImageTile), null);

        public SolidColorBrush Color
        {
            get { return GetValue(ColorProperty) as SolidColorBrush; }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(InputImageTile), null);

        public string Descritpion
        {
            get { return GetValue(DescriptionProperty) as string; }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty InputLabelProperty =
            DependencyProperty.Register("InputLabel", typeof(string), typeof(InputImageTile), null);

        public string InputLabel
        {
            get { return GetValue(InputLabelProperty) as string; }
            set { SetValue(InputLabelProperty, value); }
        }

        public static readonly DependencyProperty InputValueProperty =
            DependencyProperty.Register("InputValue", typeof(string), typeof(InputImageTile), null);

        public string InputValue
        {
            get { return GetValue(InputValueProperty) as string; }
            set { SetValue(InputValueProperty, value); }
        }

        public static readonly DependencyProperty MaxInputLengthProperty =
            DependencyProperty.Register("MaxInputLength", typeof(int?), typeof(InputImageTile), null);

        public int? MaxInputLength
        {
            get { return GetValue(MaxInputLengthProperty) as int?; }
            set { SetValue(MaxInputLengthProperty, value); }
        }

        public InputImageTile()
        {
            InitializeComponent();
            DataContext = this;
        }

        public bool IsValid() => (MaxInputLength is null) || (InputValue != null && InputValue.Length > 0);

        public bool IsEmpty() => InputValue is null;

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e) 
            => InputValue = (NameTextBox.Text.Length > 0) ? NameTextBox.Text : null;
    }
}
