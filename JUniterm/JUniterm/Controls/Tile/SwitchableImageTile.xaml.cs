﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace JUniterm.Controls.Tile
{
    public partial class SwitchableImageTile : UserControl
    {

        private bool _isFirstSelected;

        public static readonly DependencyProperty SubHeaderProperty =
            DependencyProperty.Register("SubHeader", typeof(string), typeof(SwitchableImageTile));

        public string SubHeader
        {
            get { return (string)GetValue(SubHeaderProperty); }
            set { SetValue(SubHeaderProperty, value); }
        }

        public static readonly DependencyProperty IsSwitchableProperty =
            DependencyProperty.Register("IsSwitchable", typeof(bool), typeof(SwitchableImageTile));

        public bool IsSwitchable
        {
            get { return (bool)GetValue(IsSwitchableProperty); }
            set { SetValue(IsSwitchableProperty, value); }
        }

        public static readonly DependencyProperty FirstImageSourceProperty =
            DependencyProperty.Register("FirstImageSource", typeof(string), typeof(SwitchableImageTile), null);

        public string FirstImageSource
        {
            get { return GetValue(FirstImageSourceProperty) as string; }
            set { SetValue(FirstImageSourceProperty, value); }
        }

        public static readonly DependencyProperty SecondImageSourceProperty =
            DependencyProperty.Register("SecondImageSource", typeof(string), typeof(SwitchableImageTile), null);

        public string SecondImageSource
        {
            get { return GetValue(SecondImageSourceProperty) as string; }
            set { SetValue(SecondImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(SwitchableImageTile), null);

        public SolidColorBrush Color
        {
            get { return GetValue(ColorProperty) as SolidColorBrush; }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(SwitchableImageTile), null);

        public string Descritpion
        {
            get { return GetValue(DescriptionProperty) as string; }
            set { SetValue(DescriptionProperty, value); }
        }

        public SwitchableImageTile()
        {
            InitializeComponent();
            DataContext = this;
            _isFirstSelected = true;
        }

        public void SetOperation(bool isSemicolon)
        {
                _isFirstSelected = isSemicolon;
                string source = $"pack://application:,,,{((_isFirstSelected) ? FirstImageSource : SecondImageSource)}";
                BorderImage.Source = new BitmapImage(new Uri(source));
        }

        public void ResetOperation() => SetOperation(true);

        private void BorderFrame_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(IsSwitchable)
            {
                SetOperation(!_isFirstSelected);
            }
        }

        private void BorderFrame_MouseEnter(object sender, MouseEventArgs e) => BorderFrame.Opacity = 0.9;

        private void BorderFrame_MouseLeave(object sender, MouseEventArgs e) => BorderFrame.Opacity = 1.0;

        public bool IsCommaSelected() => !_isFirstSelected;
    }
}
