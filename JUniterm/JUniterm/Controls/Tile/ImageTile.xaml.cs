﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace JUniterm.Controls
{
    public partial class ImageTile : UserControl
    {

        public static readonly DependencyProperty SubHeaderProperty =
            DependencyProperty.Register("SubHeader", typeof(string), typeof(ImageTile));

        public string SubHeader
        {
            get { return (string)GetValue(SubHeaderProperty); }
            set { SetValue(SubHeaderProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(string), typeof(ImageTile), null);

        public string ImageSource
        {
            get { return GetValue(ImageSourceProperty) as string; }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(ImageTile), null);

        public SolidColorBrush Color
        {
            get { return GetValue(ColorProperty) as SolidColorBrush; }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(ImageTile), null);

        public string Descritpion
        {
            get { return GetValue(DescriptionProperty) as string; }
            set { SetValue(DescriptionProperty, value); }
        }

        public ImageTile()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void BorderFrame_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e) => BorderFrame.Opacity = 0.9;

        private void BorderFrame_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e) => BorderFrame.Opacity = 1.0;
    }
}
