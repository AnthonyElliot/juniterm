﻿using JUniterm.Consts;
using JUniterm.Enums;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace JUniterm.Controls.Tile
{

    public partial class SelectImageTile : UserControl
    {

        public static readonly DependencyProperty SubHeaderProperty =
            DependencyProperty.Register("SubHeader", typeof(string), typeof(SelectImageTile));

        public string SubHeader
        {
            get { return (string)GetValue(SubHeaderProperty); }
            set { SetValue(SubHeaderProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(string), typeof(SelectImageTile), null);

        public string ImageSource
        {
            get { return GetValue(ImageSourceProperty) as string; }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(SolidColorBrush), typeof(SelectImageTile), null);

        public SolidColorBrush Color
        {
            get { return GetValue(ColorProperty) as SolidColorBrush; }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(SelectImageTile), null);

        public string Descritpion
        {
            get { return GetValue(DescriptionProperty) as string; }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty SelectorTypeProperty =
            DependencyProperty.Register("SelectorType", typeof(FontSelectorTypeEnum), typeof(SelectImageTile), null);

        public FontSelectorTypeEnum SelectorType
        {
            get { return (FontSelectorTypeEnum) GetValue(SelectorTypeProperty); }
            set { SetValue(SelectorTypeProperty, value); }
        }

        private object defaultValue;

        public SelectImageTile()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void LoadData(FontSelectorTypeEnum typeEnum)
        {
            switch (typeEnum)
            {
                case FontSelectorTypeEnum.FontFamily:
                    LoadFontFamiliesToComboBox();
                    break;

                case FontSelectorTypeEnum.FontSize:
                    LoadFontSizesToComboBox();
                    break;
            }
        }

        private void SelectorComboBox_Loaded(object sender, RoutedEventArgs e) 
            => LoadData(SelectorType);

        public void SetDefaultFontSize(short fontSize) 
            => defaultValue = FontConsts.GetFontSizes().Contains(fontSize) ? (object)fontSize : null;

        public void SetDefaultFontFamily(string fontFamily)
            => defaultValue = FontConsts.GetFontFamilies().Contains(fontFamily) ? (object)fontFamily : null;

        public void ClearSelectedItem() 
            => SelectorComboBox.SelectedItem = defaultValue ?? SelectorComboBox.Items.GetItemAt(0);

        private void LoadFontFamiliesToComboBox()
        {
            var fontFamilies = FontConsts.GetFontFamilies();
            fontFamilies.ForEach(x => SelectorComboBox.Items.Add(x));
            ClearSelectedItem();
        }

        private void LoadFontSizesToComboBox()
        {
            var fontSizes = FontConsts.GetFontSizes();
            fontSizes.ForEach(x => SelectorComboBox.Items.Add(x));
            ClearSelectedItem();
        }

        public object GetSelectedValue()
            => SelectorComboBox.SelectedItem;

    }
}
