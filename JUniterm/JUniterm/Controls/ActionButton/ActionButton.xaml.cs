﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace JUniterm.Controls.ActionButton
{
    public partial class ActionButton : UserControl
    {
        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(string), typeof(ActionButton), null);

        public string ImageSource
        {
            get { return GetValue(ImageSourceProperty) as string; }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(ActionButton), null);

        public string Descritpion
        {
            get { return GetValue(DescriptionProperty) as string; }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty SelectedColorProperty =
            DependencyProperty.Register("SelectedColor", typeof(SolidColorBrush), typeof(ActionButton), null);

        public SolidColorBrush SelectedColor
        {
            get { return GetValue(SelectedColorProperty) as SolidColorBrush; }
            set { SetValue(SelectedColorProperty, value); }
        }

        private Action onClick;

        public Action OnClick
        {
            get { return onClick; }
            set
            {
                onClick = value;
                OnPropertyChanged("OnClick");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ActionButton()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void ButtonTop_MouseEnter(object sender, MouseEventArgs e) => ButtonBack.Fill = SelectedColor;

        private void ButtonTop_MouseLeave(object sender, MouseEventArgs e) => ButtonBack.Fill = new SolidColorBrush(Colors.White);

        protected void OnPropertyChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        private void ButtonTop_MouseUp(object sender, MouseEventArgs e) => onClick();
    }
}
