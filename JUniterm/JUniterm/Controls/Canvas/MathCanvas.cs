﻿using System.Collections.Generic;
using System.Windows.Markup;
using System.Windows.Media;

namespace JUniterm.Controls.Canvas
{
    public class MathCanvas : System.Windows.Controls.Canvas
    {
        private List<Visual> objectList;

        protected override int VisualChildrenCount
        {
            get { return objectList?.Count ?? 0; }
        }

        public MathCanvas() : base() => objectList = new List<Visual>();

        public void AddItem(Visual obj)
        {
            objectList.Add(obj);
            base.AddVisualChild(obj);
            base.AddLogicalChild(obj);
        }

        protected override Visual GetVisualChild(int index)
            => objectList[index];

        public void ClearAll()
        {
            foreach (var item in objectList)
            {
                base.RemoveLogicalChild(item);
                base.RemoveVisualChild(item);
            }
            objectList.Clear();
        }

        public List<string> GetXAML()
        {
            List<string> list = new List<string>();
            foreach (object item in base.Children)
            {
                list.Add(XamlWriter.Save(item));
            }
            return list;
        }
    }
}
