﻿using System;
using System.Windows.Media;

namespace JUniterm.Models
{
    public class DrawingModel
    {
        public string SA { get; set; }
        public string SB { get; set; }
        public string SC { get; set; }
        public string SOp { get; set; }
        public string EA { get; set; }
        public string EB { get; set; }
        public string EC { get; set; }
        public string EOp { get; set; }
        public string Swap { get; set; }
        public FontFamily FontFamily { get; set; }
        public short FontSize { get; set; }
        public Pen Pen { get; set; }

        public bool IsOperatorsEmpty()
            => IsSOperatorsEmpty() || IsEOperatorsEmpty();

        public bool IsSOperatorsEmpty()
            => SA == "" || SOp == "" || SB == "" || SC == "";

        public bool IsEOperatorsEmpty()
            => EA == "" || EOp == "" || EB == "";

        public string GetTextForParallel()
            => $"{EA}{Environment.NewLine.ToString()}{EOp}{Environment.NewLine.ToString()}{EB}";
    }
}
