namespace JUniterm.Database
{
    using System.Data.Entity;
    using JUniterm.Database.Models;

    public partial class UnitermContext : DbContext
    {
        public UnitermContext() : base("name=UnitermContext") { }

        public virtual DbSet<Uniterm> Uniterms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) { }
    }
}
