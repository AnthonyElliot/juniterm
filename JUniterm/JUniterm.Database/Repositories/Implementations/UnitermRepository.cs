﻿using System.Collections.Generic;
using System.Linq;
using JUniterm.Database.DTOs;
using JUniterm.Database.Models;
using JUniterm.Database.Converters;

namespace JUniterm.Database.Repositories
{
    public class UnitermRepository : IUnitermRepository
    {
        private UnitermContext context;

        public UnitermRepository(UnitermContext context) => this.context = context;

        public IQueryable<Uniterm> Uniterms => context.Uniterms;

        public void Create(Uniterm uniterm)
        {
            context.Uniterms.Add(uniterm);
            context.SaveChanges();
        }

        public void Delete(Uniterm uniterm)
        {
            context.Uniterms.Remove(uniterm);
            context.SaveChanges();
        }

        public List<UnitermDTO> GetAllUnitermDTOs() => Uniterms.Select(x => new UnitermDTO
        {
            Id = x.Id,
            Name = x.Name,
            Description = x.Description
        }).ToList();

        public Uniterm GetById(int id)
            => Uniterms.Single(x => x.Id == id);

        public void Update(UnitermEditVM unitermEditVM)
        {
            var item = Uniterms.Single(x => x.Id == unitermEditVM.Id);
            UnitermConverter.Update(item, unitermEditVM);
            context.SaveChanges();
        }
    }
}
