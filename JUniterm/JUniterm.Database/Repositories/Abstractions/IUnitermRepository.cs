﻿using JUniterm.Database.DTOs;
using JUniterm.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace JUniterm.Database.Repositories
{
    public interface IUnitermRepository
    {
        IQueryable<Uniterm> Uniterms { get; }
        List<UnitermDTO> GetAllUnitermDTOs();
        void Create(Uniterm uniterm);
        Uniterm GetById(int id);
        void Delete(Uniterm uniterm);
        void Update(UnitermEditVM unitermEditVM);
    }
}