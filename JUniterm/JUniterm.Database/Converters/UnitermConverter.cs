﻿using JUniterm.Database.Models;

namespace JUniterm.Database.Converters
{
    public static class UnitermConverter
    {
        public static Uniterm GetUniterm(UnitermAddVM unitermAddVM)
            => new Uniterm
            {
                Name = unitermAddVM.Name,
                Description = unitermAddVM.Description,
                FontFamily = unitermAddVM.FontFamily,
                FontSize = unitermAddVM.FontSize,
                FirstOperator = unitermAddVM.FirstOperator,
                SecondOperator = unitermAddVM.SecondOperator
            };

        public static void Update(Uniterm uniterm, UnitermEditVM unitermEditVM)
        {
            uniterm.Name = unitermEditVM.Name;
            uniterm.Description = unitermEditVM.Description;
            uniterm.FirstOperator = unitermEditVM.FirstOperator;
            uniterm.FirstOperatorA = unitermEditVM.FirstOperatorA;
            uniterm.FirstOperatorB = unitermEditVM.FirstOperatorB;
            uniterm.FirstOperatorC = unitermEditVM.FirstOperatorC;
            uniterm.SecondOperator = unitermEditVM.SecondOperator;
            uniterm.SecondOperatorA = unitermEditVM.SecondOperatorA;
            uniterm.SecondOperatorB = unitermEditVM.SecondOperatorB;
            uniterm.SecondOperatorC = unitermEditVM.SecondOperatorC;
            uniterm.FontFamily = unitermEditVM.FontFamily;
            uniterm.FontSize = unitermEditVM.FontSize;
            uniterm.Switched = unitermEditVM.Switched;
        }

        public static UnitermEditVM GetUnitermEditVM(Uniterm uniterm)
            => new UnitermEditVM
            {
                Id = uniterm.Id,
                Name = uniterm.Name,
                Description = uniterm.Description,
                FirstOperator = uniterm.FirstOperator,
                FirstOperatorA = uniterm.FirstOperatorA,
                FirstOperatorB = uniterm.FirstOperatorB,
                FirstOperatorC = uniterm.FirstOperatorC,
                SecondOperator = uniterm.SecondOperator,
                SecondOperatorA = uniterm.SecondOperatorA,
                SecondOperatorB = uniterm.SecondOperatorB,
                SecondOperatorC = uniterm.SecondOperatorC,
                FontFamily = uniterm.FontFamily,
                FontSize = uniterm.FontSize,
                Switched = uniterm.Switched
            };

    }
}
