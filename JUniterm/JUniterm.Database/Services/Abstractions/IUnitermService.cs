﻿using JUniterm.Database.DTOs;
using JUniterm.Database.Models;
using System.Collections.Generic;

namespace JUniterm.Database.Services
{
    public interface IUnitermService
    {
        List<UnitermDTO> GetAllUnitermDTOs();
        void Add(UnitermAddVM unitermAddVM);
        Uniterm GetById(int id);
        void DeleteById(int id);
        void Update(UnitermEditVM unitermEditVM);
        UnitermEditVM GetUnitermEditVM(int id);
    }
}
