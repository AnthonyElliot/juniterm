﻿using System.Collections.Generic;
using JUniterm.Database.Converters;
using JUniterm.Database.DTOs;
using JUniterm.Database.Models;
using JUniterm.Database.Repositories;

namespace JUniterm.Database.Services
{
    public class UnitermService : IUnitermService
    {

        #region Repositories
        private readonly IUnitermRepository unitermRepository;
        #endregion

        public UnitermService(IUnitermRepository unitermRepository) 
            => this.unitermRepository = unitermRepository;

        public void Add(UnitermAddVM unitermAddVM)
        {
            var newuniterm = UnitermConverter.GetUniterm(unitermAddVM);
            unitermRepository.Create(newuniterm);
        }

        public void DeleteById(int id)
        {
            var item = unitermRepository.GetById(id);
            unitermRepository.Delete(item);
        }

        public List<UnitermDTO> GetAllUnitermDTOs() 
            => unitermRepository.GetAllUnitermDTOs();

        public Uniterm GetById(int id)
            => unitermRepository.GetById(id);

        public UnitermEditVM GetUnitermEditVM(int id)
        {
            var item = unitermRepository.GetById(id);
            return UnitermConverter.GetUnitermEditVM(item);
        }

        public void Update(UnitermEditVM unitermEditVM)
            => unitermRepository.Update(unitermEditVM);
    }
}
