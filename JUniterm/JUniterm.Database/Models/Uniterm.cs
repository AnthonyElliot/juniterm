﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JUniterm.Database.Models
{
    [Table("Uniterms")]
    public partial class Uniterm
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(10)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(50)]
        public string FirstOperatorA { get; set; }
        [StringLength(50)]
        public string FirstOperatorB { get; set; }
        [StringLength(50)]
        public string FirstOperatorC { get; set; }
        [StringLength(1)]
        public string FirstOperator { get; set; }
        [StringLength(50)]
        public string SecondOperatorA { get; set; }
        [StringLength(50)]
        public string SecondOperatorB { get; set; }
        [StringLength(50)]
        public string SecondOperatorC { get; set; }
        [StringLength(1)]
        public string SecondOperator { get; set; }
        [StringLength(50)]
        public string FontFamily { get; set; }
        public short FontSize { get; set; }
        [StringLength(1)]
        public string Switched { get; set; }
    }
}
