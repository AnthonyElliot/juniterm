﻿using System.ComponentModel.DataAnnotations;

namespace JUniterm.Database.Models
{
    public class UnitermAddVM
    {
        [StringLength(10)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(50)]
        public string FontFamily { get; set; }
        public short FontSize { get; set; }
        [StringLength(1)]
        public string FirstOperator { get; set; }
        [StringLength(1)]
        public string SecondOperator { get; set; }
    }
}
