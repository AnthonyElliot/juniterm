﻿using System.ComponentModel.DataAnnotations;

namespace JUniterm.Database.Models
{
    public class UnitermEditVM
    {
        public int Id { get; set; }
        [StringLength(10)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(50)]
        public string FirstOperatorA { get; set; }
        [StringLength(50)]
        public string FirstOperatorB { get; set; }
        [StringLength(50)]
        public string FirstOperatorC { get; set; }
        [StringLength(1)]
        public string FirstOperator { get; set; }
        [StringLength(50)]
        public string SecondOperatorA { get; set; }
        [StringLength(50)]
        public string SecondOperatorB { get; set; }
        [StringLength(50)]
        public string SecondOperatorC { get; set; }
        [StringLength(1)]
        public string SecondOperator { get; set; }
        [StringLength(50)]
        public string FontFamily { get; set; }
        public short FontSize { get; set; }
        [StringLength(1)]
        public string Switched { get; set; }
    }
}
